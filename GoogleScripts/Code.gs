
/**
 * Copyright 2017, Scribe Software, Inc.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *By using these, you acknowledege the following:
 *
 * - This document contains programming examples provided by Scribe for illustrative
 *   purposes only. Scribe grants you a nonexclusive copyright license to use all 
 *   programming code examples from which you can generate similar functionality tailored
 *   to your own specific needs. 
 *
 * - These examples have not been thoroughly tested under all conditions and are provided
 *   to you "AS IS" without any warranties of any kind. Therefore, Scribe cannot guarantee
 *   or imply reliability, serviceability, or functionality of these programs. The implied 
 *   warranties of non-infringement, merchantability, and fitness for a particular purpose 
 *   are expressly disclaimed.
 *
 *Licence agreement can be found here: 
 *   https://success.scribesoft.com/s/article/ka632000000GmxZAAS/Scribe-Software-Tool-Kit-And-Technology-License-Agreement
 *
 * This is a simple Documentation Integration Tool developed in Google Sheets against Scribe's Rest API
 * 
 */
 
 
 /* Version 3 by "front of building coder" 8-21-2017
              - added ability to have maps and solutions of the same name 
              - added support for Request Reply without Input Parameters 
              - added support for Message Maps 
              - Cleaned up cluncky picklist code 
 */ 

//Global Variables
// Credentials to call Scribe API
    var APIURL; 
    var USER; 

//Setting Up the Menu System in Google Sheets Container 
function onOpen() {
    SpreadsheetApp.getUi() // Or DocumentApp
        .createMenu('Scribe')
        .addItem('Document Solutions', 'openDialog')
        //.addItem('Reset', 'doReset')
        .addToUi(); 
}

function openDialog() {
    //Clean up any data from the last run!
    doReset();
    var html = HtmlService.createTemplateFromFile('DocumentSolutions')
        .evaluate() // evaluate MUST come before setting the NATIVE mode
        .setSandboxMode(HtmlService.SandboxMode.NATIVE).setHeight(500);
     
     SpreadsheetApp.getUi() // 
        .showModalDialog(html, 'Scribe Documentation Tool');
}

function include(filename) {
    return HtmlService.createHtmlOutputFromFile(filename)
    .getContent();
}

function formProcessor(formObject){
    APIURL = formObject.environment; 
    var form_context = formObject.form_context;
    var orgId = formObject.org; 
    var solutionId = formObject.solution; 
    var errorRuns = formObject.errorRuns; 
    var org; 
    var orgs = []; 
    var block; 
    var blocks =[]; 
    
    doReset(); //Housekeeping of the workbook prior to each run 
    USER = {username: formObject.username, password: formObject.password};  
  
  // Form Context is used passed between client and server for simple state management 
    switch (form_context) {
        case "Connect":
            Logger.log("Called Connect for user " + USER.username ); // Useful debug trace
            orgs = getScribeOrgs(); 
            return orgs;
        break; 
    
        case "Solution":
            Logger.log("Called Solutions for user " + USER.username + "With org ID - " + orgId); // Useful debug trace
            solutions =  getScribeSolutionsListByOrgId(orgId);
            return solutions; 
         break;

        case "Document":
             org = getScribeOrg(orgId); 
            outputOrg(org); 
            // Generate Solution File 
            solution = getScribeSolution(org, solutionId);
            createSheet("Solution Details"); 
            outputSolution(solution); 
            //Output the Maps for the Solution 
            maps = getScribeMaps(org, solution); 
    
            //For each Map in Maps Create A WorkSheet
            for (var i =0; i < maps.length; i++){
                map = maps[i]; 
                outputMap(map); 
                //Get the Blocks in the Current Map 
                blocks = getScribeBlocks(org.id, solution.id, map.id);
                Logger.log(JSON.stringify(blocks));
               
                var retList = [];
                var startingBlock = blocks[0];
                // Sort the Blocks to Match the SOL UX - This will not be requried Post 3/31/2017 Granite Release
                visitAllDescendants(blocks, startingBlock, retList);
                outputBlocks(retList); 
                
            }
    
        //Building the List of Solutions available for Documentation and sending back to the DocumentSolutions Form 
        orgs = getScribeOrgs();    
    } 
   
    return orgs

  }

//========================= SCRIBE API GET FUNCTIONS =============================
function getScribeBlocks(orgId, solutionId, mapId){
    var method; 
    var map; 
    var dataSet = []; 
    var block; 
    var blocks = [];
    var url = APIURL + orgId + "/solutions/" + solutionId + "/maps/" + mapId; 
  
    method = "Get"; 
    dataSet = callScribeApi(url, method); 
    blocks = dataSet.blocks; 
   
    for (var i = 0; i < dataSet.length; i++) {
        block = dataSet[i];
            blocks.push({id: block.id, 
            name: block.name,
            entity: block.entity,
            label: block.label,
            x: block.x, 
            blockType: block.blockType, 
            fieldMappings: block.fieldMappings,
            requestData: block.requestData,
            expressionItems: block.expressionItems,
            filterItems: block.filterItems,
            parentBlockId: block.parentBlockId});   
    }
  
    return blocks; 
}

function getScribeMaps(org, solution){
    var url = APIURL + org.id + "/solutions/" + solution.id + "/maps/"; 
    var primaryContact; 
    var maps =[]; 
    var map; 
    var dataSet = []; 
    
    method = "Get"; 
    dataSet = callScribeApi(url, method); 
       
    for (var i = 0; i < dataSet.length; i++) {
        map = dataSet[i];
        maps.push({id: map.id, name: map.name, description: map.description,netChange: map.netChange, netChangeFieldName: map.netChangeFieldName,lastModificationDate: map.lastModificationDate}); 
    }
    
    return maps; 
}

function getScribeOrg(orgId){
    var url = APIURL + orgId; 
    var primaryContact; 
    var org; 
    var data;
    var dataSet;
    var method; 
    
    method = "Get"; 
    dataSet = callScribeApi(url, method); 
  
    primaryContact = dataSet.primaryContactFirstName + " " + dataSet.primaryContactLastName; 
    
    org = {id: dataSet.id, 
           name: dataSet.name, 
           primaryContact: primaryContact, 
           primaryContactEmail: dataSet.primaryContactEmail, 
           administrators: dataSet.administrators,
           isSourceDataLocal: dataSet.isSourceDataLocal, 
           apiToken: dataSet.apiToken, 
           securityRules: dataSet.securityRules, 
           parentId: dataSet.parentId};   
 
    return org; 
}

function getScribeOrgs(){
    var url = APIURL + "?noPagination=true"; 
    var primaryContact; 
    var orgs =[]; 
    var org; 
    var dataSet = []; 
    
    method = "Get"; 
    dataSet = callScribeApi(url, method); 
    
    for (var i = 0; i < dataSet.length; i++) {
        org = dataSet[i];
        primaryContact = org.primaryContactFirstName + ' ' + org.primaryContactLastName
        //Push a simple ORG object
        orgs.push({id: org.id, name: org.name, primaryContact: primaryContact,parentId: org.parentId });   
    }
  
    return orgs; 
}
               
function getScribeSolution(org, solutionId){
    var solution; 
    var lastmodifiedUser; 
    var lastmodified;  
    var dataSet; 
    var data; 
    var method; 
    var mapLinks = []; 

    var url = APIURL + org.id + "/solutions/" + solutionId;
    method = "Get"; 
    dataSet = callScribeApi(url, method);   
  
    if (dataSet.solutionType != "Replication"){ 
        //Iternate the Maplinks and get the last mod rolling up at the solution from the Maps Collection 
        lastmodified = "01/01/2000"; 
        mapLinks = dataSet.mapLinks; 
        for (var ml = 0; ml < mapLinks.length; ml++) { 
            if (lastmodified < mapLinks[ml].lastModified){
                lastmodifiedUser = mapLinks[ml].modifiedBy;
                lastmodified = mapLinks[ml].lastModified;  
            }
        }
    }
    else {
        lastmodifiedUser = "RS - N/A"; 
        lastmodified = "RS - N/A"; 
    } 
    
    solution = {id: dataSet.id, 
              name: dataSet.name,  
              status: dataSet.status, 
              lastRunTime: dataSet.lastRunTime, 
              nextRunTime: dataSet.nextRunTime, 
              isDisabled: dataSet.isDisabled, 
              reasonDisabled: dataSet.reasonDisabled, 
              minAgentVersion: dataSet.minAgentVersion, 
              mapLinks: dataSet.mapLinks,
              modifiedBy: lastmodifiedUser, 
              lastModified: lastmodified};    
  
    return solution;                    
}

function getScribeSolutionsListByOrgId(orgId){
    var url = APIURL + orgId + "/solutions" ;
    var solutions = []; 
    var solution; 
    var dataSet =[]; 
    var method; 
    
    method = "Get"; 
    dataSet = callScribeApi(url, method); 
  
    for (var i = 0; i < dataSet.length; i++) {
        solution = dataSet[i];
        solutions.push({id: solution.id, name: solution.name}); 
    }
  
    return solutions;                    
}

//=============================  OUTPUT STATEMENTS  =====================================
function outputBlocks(blocks){
    //TODO:  Add a function to order blocks by parentID (double linked list required)
    var offset; 
    var flowOffset; 
    var columnCount; 
    var rows= []; 
    var block; 
    var lastrow; 
    var range; 
    var cols = 6; 
    //Setup the columns presentation for the sheet that the Blocks are being added to 
    ss = SpreadsheetApp.getActiveSpreadsheet();
    sheet = ss.getActiveSheet();
      sheet.setColumnWidth(1, 200);
      sheet.setColumnWidth(2, 150);
      sheet.setColumnWidth(3, 200);
      sheet.setColumnWidth(4, 200);
      sheet.setColumnWidth(5, 400);
      sheet.setColumnWidth(6, 50);
  
      //Set the Cell Alignment to Left
      
      range = sheet.getRange("A1:F1");
      range.setHorizontalAlignment("left");
  
  
    for (var z = 0; z < blocks.length; z++){
        block = blocks[z];
        flowOffset == 1; 
        ss = SpreadsheetApp.getActiveSpreadsheet();
        sheet = ss.getActiveSheet();
        //Add a row to make it easier to read, you can comment the next line out for compact format!
        sheet.appendRow([" "]);//If you don't add a space future calls to getLastRow or similiar will ignore the row - 
        
     
            switch (block.blockType) {
                case "Upsert":
                case "Update":
                case "Insert":
                case "UpdateInsert":
                case "Create": 
                    offset = 3;
                    columnCount = 3;
                    sheet.appendRow([block.label,block.blockType,'Field Name', 'Type', 'Formula' ]);
                    formatHeader(cols);
                    rows = outputFieldMap(block); 
                    
                    insertRows(offset, rows, columnCount) 
                break;
            
                case "BuildReply":  
                    offset = 3;
                    columnCount = 3;
                    sheet.appendRow([block.label,block.blockType,'Field Name', 'Type', 'Formula' ]);
                    formatHeader(cols);
                    rows = outputFieldMap(block); 
                    insertRows(offset, rows, columnCount) 
                break;
                
                case  "NativeQuery":
                    offset = 1;
                    columnCount = 4;
                    sheet.appendRow([block.label, block.entity, block.blockType]); 
                    formatHeader(cols);
                    rows = outputNativeQuery(block);
                    insertRows(offset, rows, columnCount) 
                break;
                
                case  "WaitForRequest":
                    offset = 3;
                    columnCount = 3;
                    sheet.appendRow([block.label, block.entity, 'Input Params','DataType', 'Debug Values']); 
                    formatHeader(cols);
                    rows = outputRequestData(block);
                    insertRows(offset, rows, columnCount) 
                break;
                
                case  "ForEach":
                case  "ForEachChild":
                    offset = 3;
                    columnCount = 1;
                    sheet.appendRow([block.label, block.blockType,block.name ]); 
                    formatHeader(cols);
                    rows = outputForEachData(block);
                    insertRows(offset, rows, columnCount) 
                break;
            
                case  "Group": 
                case  "Continue":
                case  "Comment":
                case  "Loop":
                case  "LoopExit": 
                case  "MapExit":
                case  "WaitForMessage":
                    offset = 3;
                    columnCount = 1;
                    sheet.appendRow([block.label, block.blockType,block.name ]); 
                    formatHeader(cols);
                    rows = outputDescriptionData(block);
                    insertRows(offset, rows, columnCount) 
                break;
                
                case  "IfElse":
                    offset = 3;
                    columnCount = 4;
                    sheet.appendRow([block.label, block.blockType,block.name]);
                    formatHeader(cols);
                    rows = outputIfElse(block);
                    insertRows(offset, rows, columnCount) 
                break;
                
                case  "Fetch":
                case  "Query":
                case  "Lookup":
                case  "Delete": 
                        
                        offset = 3;
                        columnCount = 4;
                        sheet.appendRow([block.label, block.blockType, 'Field', 'Operator','Value', 'And/Or' ]);
                        formatHeader(cols);
                        rows = outputFilterItems(block);
                        insertRows(offset, rows, columnCount) 
                break;
        
                default: 
                        Logger.log("No Support for Block Type: " + block.blockType); 
                break;
            }
                
    }
 
}

function outputFieldMap(block){  
    var rows = []; 
    var i; 
    var fieldMap; 
    var fieldMappings =[];
  
    fieldMappings = block.fieldMappings; 
        
        for (i =0; i < fieldMappings.length; i ++){
            fieldMap = fieldMappings[i];      
            rows.push([fieldMap.targetField,fieldMap.targetDataType,fieldMap.targetFormula]);  
        }
    
    return rows; 
}

function outputForEachData(block){  
    var rows = []; 
    rows.push([block.name]);         
    
    return rows; 
}

function outputDescriptionData(block){  
    var rows = []; 
    rows.push(["'" + block.description + "'"]);         
    
    return rows; 
}

function outputRequestData(block){  
    var rows = []; 
    var i; 
    var requestData =[]; 
    var request;
    
    requestData = block.requestData; 
    
    Logger.log("Request Data Length = " + requestData.length);
          
        for (var i = 0; i < requestData.length; i ++){
            request = requestData[i]; 
            rows.push([request.name, request.dataType, request.sampleValue]);  
        }          
    
    return rows; 
}

function outputNativeQuery(block){  
    var rows = []; 
    var i; 
    var nativeQuery; 
    
    nativeQuery = block; 
          
    rows.push([nativeQuery.label, nativeQuery.entity, nativeQuery.blockType, nativeQuery.queryText]);  
                 
    return rows; 
}


function outputIfElse(block){  
    var rows = []; 
    var i;  
    var expressionItem;
    var expressionItems = [];
        
    expressionItems = block.expressionItems; 
         
    if (expressionItems.length == 0){
        rows.push(["no expression item paramaters", "", "", ""]); 
     }
     else {
            for (var i =0; i < expressionItems.length; i ++){
                expressionItem = expressionItems[i];    
                rows.push([expressionItem.conditionOperator, expressionItem.leftExpression, expressionItem.logicalOperator,expressionItem.rightExpression]);  
 
            }     
         }

    return rows; 
}

function outputFilterItems(block){  
    var rows = []; 
    var i;  
    var filterItem;
    var filterItems =[]; 
    var logicalOperator; 
    filterItems = block.filterItems
       
    if (filterItems.length == 0){
        rows.push(["no filter paramaters", "", "", ""]); 
     }
    else {
        for (var i =0; i < filterItems.length; i ++){
            filterItem = filterItems[i];         
            rows.push([filterItem.fieldName, filterItem.comparisonOperator, filterItem.compareValue,filterItem.logicalOperator]);  
        }   
    } 
    
    return rows; 
}

function outputOrg(org) { 
    var ss = SpreadsheetApp.getActiveSpreadsheet();
    var sheets = ss.getSheets();
    var sheet = ss.getActiveSheet();
    var primaryContact; 
    var rows = []; 
    var offset = 1; 
    var columnCount = 2;
    var securityRule; 
    var securityRules =[]; 
    var range; 
  
    securityRules = org.securityRules
  
    sheet.setName("Org Details"); 
    sheet.setColumnWidth(1, 200);
    sheet.setColumnWidth(2, 200);
    sheet.setColumnWidth(3, 200);
    sheet.setColumnWidth(4, 200);
    sheet.setColumnWidth(5, 200);
  
    sheet.appendRow(["Org Name              ", org.name]);
    sheet.appendRow(["Org ID                ", org.id]);
    sheet.appendRow(["Primary Contact       ", org.primaryContact]);
    sheet.appendRow(["Primary Contact Email ", org.primaryContactEmail]);
    sheet.appendRow(["Administrators        ", org.administrators.toString()]);
    sheet.appendRow(["Source Data Local     ", org.isSourceDataLocal]);
    sheet.appendRow(["API Token             ", org.apiToken]);
  
    //Modify the Header area to look a bit better
    range = sheet.getRange("A1:A7");
    range.setBackground("#d3d3d3"); //Light grey
    range.setFontFamily('Roboto'); 
    range.setFontSize(10); 
    //Modify the data area to look a bit better    
    range = sheet.getRange("B1:B7");
    range.setFontFamily('Roboto'); 
    range.setFontSize(10); 
    range.setHorizontalAlignment("left");
    range.setVerticalAlignment("top")

    //  Loop The Security Rules and Prepare them for Output  
    range = sheet.getRange("A10:E10");
    sheet.appendRow(['Security Access Information']);      
        
    if (securityRules.length == 0){
        rows.push(["no security rules have been established", "", "", ""]); 
     }
    else {
        sheet.appendRow(['Name','API Access ', 'Event Access' ,'IP Range Start', 'IP Range End']);
        var cols = 5; 
        formatHeader(cols);
     
        for (var i =0; i < securityRules.length; i ++){
            securityRule = securityRules[i];         
            rows.push([securityRule.name, 
                       securityRule.apiAccessEnabled, 
                       securityRule.eventSolutionAccessEnabled, 
                       securityRule.allowedIpRangeStartAddress,
                       securityRule.allowedIpRangeEndAddress]);    
        }   
    
        offset = 1;
        columnCount = 5;
        insertRows(offset, rows, columnCount) 
    }   

}

function outputSolution(solution) { 
    var ss = SpreadsheetApp.getActiveSpreadsheet();
    var sheet = ss.getSheetByName("Solution Details");
    var rows = []; 
    var offset = 1; 
    var columnCount = 2;
    var mapLink; 
    var mapLinks =[]; 
    var range; 
  
    mapLinks = solution.mapLinks
  
    sheet.setColumnWidth(1, 200);
    sheet.setColumnWidth(2, 200);
    sheet.setColumnWidth(3, 200);
    sheet.setColumnWidth(4, 200);
    sheet.setColumnWidth(5, 500);
    
    sheet.appendRow(["Solution Name         ", solution.name]);
    sheet.appendRow(["Solution Id           ", solution.id]);
    sheet.appendRow(["Status                ", solution.status]);
    sheet.appendRow(["Last Run Time         ", solution.lastRunTime]);
    sheet.appendRow(["Next Run Time         ", solution.nextRunTime]);
    sheet.appendRow(["Is Disabled           ", solution.isDisabled]);
    sheet.appendRow(["Reason Disabled       ", solution.reasonDisabled]);
    sheet.appendRow(["Min Agent Version     ", solution.minAgentVersion]);
    sheet.appendRow(["Modified By           ", solution.modifiedBy]);
    sheet.appendRow(["Last Modified         ", solution.lastModified]);
  
    //Modify the Header area to look a bit better
    range = sheet.getRange("A1:A10");
    range.setBackground("#d3d3d3"); //Light grey
    range.setFontFamily('Roboto'); 
    range.setFontSize(10); 
    //Modify the Data area to look a bit better  
    range = sheet.getRange("B1:B10");
    range.setFontFamily('Roboto'); 
    range.setFontSize(10); 
    range.setHorizontalAlignment("left");
    range.setVerticalAlignment("top")
  
    //  Loop The Security Rules and Prepare them for Output  
    range = sheet.getRange("A12:E12");
    sheet.appendRow(['Map Links Information']);      
        
    if (mapLinks.length == 0){
        rows.push(["no map link details", "", "", ""]); 
     }
    else {
        sheet.appendRow(['Name','Type','Sources ', 'Targets' , 'End Point']);
        var cols = 5; 
        formatHeader(cols);

            for (var i =0; i < mapLinks.length; i ++){
                mapLink = mapLinks[i];         
                rows.push([mapLink.name, mapLink.mapType, mapLink.sources, mapLink.targets,mapLink.eventUrl]);    
             }   
        
        offset = 1;
        columnCount = 5;
        insertRows(offset, rows, columnCount) 
    }     
}

function outputMap(map){
    //Output Map Details 
    var range; 
    createSheet(map.name); 
    ss = SpreadsheetApp.getActiveSpreadsheet();
    sheet = ss.getActiveSheet();
    sheet.appendRow(["Map Name        ", map.name]);
    sheet.appendRow(["Map ID          ", map.id]);
    sheet.appendRow(["Desc            ", map.description]);
    sheet.appendRow(["Net Change      ", map.netChange]);
    sheet.appendRow(["Net Change Field", map.netChangeFieldName]);
    sheet.appendRow(["Last Modified   ", map.lastModificationDate]);
  
    //Modify the Header area to look a bit better
    range = sheet.getRange("A1:A6");
    range.setBackground("#d3d3d3"); //Light grey
    range.setFontFamily('Roboto'); 
    range.setFontSize(10); 
    //Modify the Data area to look a bit better   
    range = sheet.getRange("B1:B6");
    range.setFontFamily('Roboto'); 
    range.setFontSize(10); 
    range.setHorizontalAlignment("left");
    range.setVerticalAlignment("top");

}

function createSheet(sheetName){
    var sheet;
    var ss = SpreadsheetApp.getActiveSpreadsheet();
    ss.insertSheet(sheetName);
    sheet = ss.getSheetByName(sheetName); 
    sheet.activate(); 
 }
  

//=================================  Utilities Section  =================================== 
function insertRows(offset, rows, columnCount) {

    if (rows != 0){
      var ss = SpreadsheetApp.getActiveSpreadsheet();
      var sheets = ss.getSheets();
      var sheet = ss.getActiveSheet();
      var range; 
      var lastRow; 
  
        if (sheet.getLastRow == 0){
          lastRow = 1; 
        } 
        else {
          lastRow = sheet.getLastRow();
          lastRow += 1;
        } 
  
      range = sheet.getRange(lastRow, offset, rows.length, columnCount); 
      range.setFontFamily('Roboto'); 
      range.setFontSize(10); 
      range.setHorizontalAlignment("left");
      range.setVerticalAlignment("top") 
      range.setValues(rows);
    }
}

function formatHeader(cols){
    var ss;
    var sheet; 
    var range; 
    ss = SpreadsheetApp.getActiveSpreadsheet();
    sheet = ss.getActiveSheet();
    range = sheet.getRange(sheet.getLastRow(), 1, 1, cols);//sheet.getLastColumn()
    range.setBackground("#d3d3d3"); //Light grey
    range.setFontFamily('Roboto'); 
    range.setFontSize(10); 
    range.setFontWeight("bold");
    range.setHorizontalAlignment("left");
}

//Reset the Spreadsheet Between Runs for Ease of Use 
function doReset(){
    var ss = SpreadsheetApp.getActiveSpreadsheet();
    var a = ss.getSheets();
  
    for (i = 0; i < a.length; ++i) {
        //Loop the sheets array and activate the current sheet
        a[i].activate(); 
        a[i].clear();
        //Do not delete the first sheet 
        if (i > 0){ 
            ss.deleteActiveSheet(); 
        }  
    }
}

function callScribeApi(APIURL, method){ 
    var headers = { "Authorization" : "Basic " + Utilities.base64Encode(USER.username + ':' + USER.password)};
    var params = {"method":method,"headers":headers,"muteHttpExceptions": true,"contentType":"application/json", "followRedirects":true};  
  
    try
    {
        response = UrlFetchApp.fetch(APIURL, params);
        var resGetContentText = response.getContentText()
        var responseData = JSON.parse(resGetContentText); 
    }
  
    catch(err)
  
    {
        Logger.log("This is the error trap response: " + err);
        Logger.log("Reponse Code = " + response.getResponseCode()); 
        Logger.log("response body = " + response.getContentText()); 
    } 
   
  return responseData;  
} 
 
 //Functions requried to reorder the maps, can be removed by 3/31 after the Granite Release
   
 function getBlock(blocks, id) {
    var returnBlock = null;
    for (var i = 0; i < blocks.length; i++) {
        if (blocks[i].id === id) {
            returnBlock = blocks[i];
            break;
        }
    }
    return returnBlock;
};
 
// The next function is what does all the work to order the list returning an ordered list
function visitAllDescendants(blocks, b, orderedList) {

    while (b) {
        orderedList.push(b);
        if (b.hasOwnProperty("firstChildBlockId")) {
            var firstChildBlockId = b["firstChildBlockId"];
            var bc = getBlock(blocks, firstChildBlockId);
            if (bc) {
                visitAllDescendants(blocks, bc, orderedList);
            }
        }
        if (b.hasOwnProperty("firstChildBlockContainer1Id")) {
            var firstChildBlockContainer1Id = b["firstChildBlockContainer1Id"];
            var bc1 = getBlock(blocks, firstChildBlockContainer1Id);
            if (bc1) {
                visitAllDescendants(blocks, bc1, orderedList);
            }
        }
        if (b.hasOwnProperty("firstChildBlockContainer2Id")) {
            var firstChildBlockContainer2Id = b["firstChildBlockContainer2Id"];
            var bc2 = getBlock(blocks, firstChildBlockContainer2Id);
            if (bc2) {
                visitAllDescendants(blocks, bc2, orderedList);
            }
        }
        var nextBlockId = b["nextBlockId"];
        b = getBlock(blocks, nextBlockId);
    }
};


