## DISCLAIMER ##
Copyright (c) 2019, TIBCO Software Inc. All rights reserved. 
No Support. You acknowledge that TIBCO will not provide any maintenance or support for this software. You have no right to receive any upgrades, patches, enhancements, bug fixes, new versions or new releases of the software. 
Internal Use Only License Grant. TIBCO hereby grants you a limited, non‐transferable, non‐exclusive license to use the software solely for your internal business purposes. The name of TIBCO Software Inc. may not be used to endorse or promote products derived from this software without specific prior written permission of TIBCO Software Inc. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT OWNERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

## Google Spreadsheet Documentation Tool 3.0##

If you are creating Solutions and Maps for a Customer, doing a Proof of Concept project, or simply tracking a large project, you may want to document the contents of those Solutions and Maps as part of your development process. Scribe Labs — Google Docs - Documentation Tool Application is an open source third-party product that leverages Google Docs to help you easily document your Scribe Online Solutions and Maps. This application is an example of how you can use the Scribe Online API to create your own applications.

Further details on the use of this can be found at:

https://dev.scribesoft.com/en/g_docs/doc_maps.htm

https://dev.scribesoft.com

